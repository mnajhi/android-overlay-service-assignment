package com.android.assignment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public final class MainActivity extends Activity {
	Intent overlayIntent = new Intent();
	@Override
	protected void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ViewFactory.getInstance().init(this);
//		overlayIntent.setClass(this, OverlayService.class);
//		this.startService(overlayIntent);
//		finish();
		
	}
	
	public void startService(View view){
		stopService((View)null);
		overlayIntent.setClass(this, OverlayService.class);
		this.getApplicationContext().startService(overlayIntent);
//		finish();
		this.moveTaskToBack(true);
	}
	
	public void stopService(View view){
		overlayIntent.setClass(this, OverlayService.class);
		this.stopService(overlayIntent);
	}
}
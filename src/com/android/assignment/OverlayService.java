package com.android.assignment;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.app.Service;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.Toast;

import com.android.assignment.ui.viewholder.EdgeBubbleViewHolder;
import com.android.assignment.ui.viewholder.EdgeButtonsViewHolder;

public final class OverlayService extends Service implements OnClickListener {
	private final String TAG = "OverlayService";

	private static final int LayoutParamFlags = WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH
			| WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
			| WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
			| WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
			| WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
			| WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS;

	private EdgeBubbleViewHolder edgeViewHolder = null;
	private EdgeButtonsViewHolder edgeButtonsViewHolder = null;
	private WindowManager windowManager;
	private WindowManager.LayoutParams paramsBubble = null;
	private WindowManager.LayoutParams paramsButtons = null;

	private View.OnClickListener clickListener;

	private boolean isBubbleOpen = false;
	private boolean isContactsListOpen = false;

	private final int MAX_MOVE_FROM_RIGHT_TO_LEFT = 0;
	private final int MIN_MOVE_FROM_RIGHT_TO_LEFT = (int) (329 * ViewFactory
			.getInstance().getWidthRatio());

	private final int MAX_MOVE_Y = (int) (400 * ViewFactory.getInstance()
			.getHeightRatio());
	private final int MIN_MOVE_Y = (int) (-240 * ViewFactory.getInstance()
			.getHeightRatio());

	private int selectedContact = -1;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		windowManager = (WindowManager) this.getSystemService(WINDOW_SERVICE);
		initButtonsLayout();
		initBubbleLayout();
		onEdgeBubbleClose();

	}

	private void initBubbleLayout() {
		paramsBubble = new WindowManager.LayoutParams(
				WindowManager.LayoutParams.MATCH_PARENT,
				WindowManager.LayoutParams.WRAP_CONTENT,
				WindowManager.LayoutParams.TYPE_PRIORITY_PHONE,
				LayoutParamFlags
						| WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
				PixelFormat.TRANSLUCENT);

		paramsBubble.gravity = Gravity.TOP | Gravity.LEFT;
		paramsBubble.x = MIN_MOVE_FROM_RIGHT_TO_LEFT;
		paramsBubble.y = (int) (50 * ViewFactory.getInstance().getHeightRatio());
		LayoutInflater inflater = LayoutInflater.from(this);
		View layoutView = inflater.inflate(R.layout.edge_bubble, null);
		edgeViewHolder = new EdgeBubbleViewHolder(layoutView);

		ViewFactory.getInstance().scaleView(edgeViewHolder.rootView);
		windowManager.addView(edgeViewHolder.rootView, paramsBubble);

		edgeViewHolder.imgClose.setOnClickListener(this);
		edgeViewHolder.imgCall.setOnClickListener(this);
		edgeViewHolder.imgMsg.setOnClickListener(this);
		edgeViewHolder.imgCloseContacts.setOnClickListener(this);

		for (int index = 0; index < edgeViewHolder.rltContacts.size(); index++) {
//			edgeViewHolder.rltContacts.get(index)
//					.setTag(Integer.valueOf(index));
			edgeViewHolder.rltContacts.get(index).setOnClickListener(
					new OnClickListener() {
						@Override
						public void onClick(View v) {
							onContactClicked(((ContactProperties) v.getTag(R.string.contact_properties_key)).getPosition());
						}
					});
		}
		edgeViewHolder.rootView.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View view, MotionEvent arg1) {
				Log.e(TAG, "Touched");
				return false;
			}
		});

	}

	private void initButtonsLayout() {
		paramsButtons = new WindowManager.LayoutParams(
				WindowManager.LayoutParams.WRAP_CONTENT,
				WindowManager.LayoutParams.WRAP_CONTENT,
				WindowManager.LayoutParams.TYPE_PRIORITY_PHONE,
				LayoutParamFlags, PixelFormat.TRANSLUCENT);
		paramsButtons.gravity = Gravity.TOP | Gravity.RIGHT;
		paramsButtons.x = 0;
		paramsButtons.y = (int) (110 * ViewFactory.getInstance()
				.getHeightRatio());
		LayoutInflater inflater = LayoutInflater.from(this);
		View layoutView = inflater.inflate(R.layout.edge_buttons, null);
		edgeButtonsViewHolder = new EdgeButtonsViewHolder(layoutView);
		ViewFactory.getInstance().scaleView(layoutView);
		windowManager.addView(layoutView, paramsButtons);
		edgeButtonsViewHolder.showContactDrag.setOnClickListener(clickListener);
		edgeButtonsViewHolder.showContactDrag.setOnTouchListener(touchListener);
		edgeButtonsViewHolder.showContactsList
				.setOnTouchListener(touchListener);

	}

	private View.OnTouchListener touchListener = new View.OnTouchListener() {
		private int initialX;
		private int initialY;
		private float initialTouchX;
		private float initialTouchY;
		private int deltaMoveX;
		private int deltaMoveY;

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				initialX = paramsBubble.x;
				initialY = paramsBubble.y;
				initialTouchX = event.getRawX();
				initialTouchY = event.getRawY();
				deltaMoveX = 0;
				deltaMoveY = 0;
				hideButtons();
				return true;
			case MotionEvent.ACTION_UP:
				if (v.getId() == R.id.showContactsList) {
					if (!isBubbleOpen && !isContactsListOpen) {
						deltaMoveX = (int) (initialTouchX - event.getRawX());
						if (deltaMoveX > (100 * ViewFactory.getInstance()
								.getWidthRatio())) {
							showContactsList();
						} else {
							showButtons();
						}
					}
				} else {
					if (!isContactsListOpen && !isBubbleOpen) {
						updateBubbleView();
					} else if (isBubbleOpen) {
						deltaMoveX = (int) (event.getRawX() - initialTouchX);
						deltaMoveY = (int) (event.getRawY() - initialTouchY);
						if (deltaMoveX > deltaMoveY
								&& deltaMoveX > (150 * ViewFactory
										.getInstance().getWidthRatio())) {
							startPopupCloseAnimation();
						}
					}
				}

				return true;
			case MotionEvent.ACTION_MOVE:
				int xMove = initialX + (int) (event.getRawX() - initialTouchX);
				int yMove = initialY + (int) (event.getRawY() - initialTouchY);
				if (v.getId() != R.id.showContactsList) {
					if (!isBubbleOpen && !isContactsListOpen) {
						if (xMove > MAX_MOVE_FROM_RIGHT_TO_LEFT
								&& xMove < MIN_MOVE_FROM_RIGHT_TO_LEFT) {
							if (edgeViewHolder.rootView.getVisibility() == View.GONE) {
								edgeViewHolder.rootView
										.setVisibility(View.VISIBLE);
							}
							paramsBubble.x = xMove;
							windowManager.updateViewLayout(
									edgeViewHolder.rootView, paramsBubble);
							Log.e("touch", "" + event.getRawX() + " : "
									+ paramsBubble.x);
						}
					} else {
						if (yMove < MAX_MOVE_Y && yMove > MIN_MOVE_Y) {
							paramsBubble.y = yMove;
							windowManager.updateViewLayout(
									edgeViewHolder.rootView, paramsBubble);
							Log.e("touch", "" + event.getRawY() + " : "
									+ paramsBubble.y);
						}
					}
				}

				return true;
			}
			return false;
		}

	};

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		return START_STICKY;
	}

	private void showContactsList() {
		isContactsListOpen = true;
		selectedContact = -1;
		edgeViewHolder.lnrContacts.setVisibility(View.VISIBLE);
		edgeViewHolder.imgCloseContacts.setScaleX(1);
		edgeViewHolder.imgCloseContacts.setScaleY(1);

		Animation fadeIn1 = AnimationHelper.getInstance().getFadeAnimation(0,
				1, 1000, null, null);
		Animation rotation1 = AnimationHelper.getInstance()
				.getRotationAnimation(0, -360, 1000,
						new DecelerateInterpolator(), null);

		final AnimationSet animation1 = new AnimationSet(false); // change to
																	// false
		animation1.addAnimation(fadeIn1);
		animation1.addAnimation(rotation1);
		edgeViewHolder.imgCloseContacts.startAnimation(animation1);

		for (int i = 0; i < edgeViewHolder.rltContacts.size(); i++) {
			edgeViewHolder.rltContacts.get(i).setAlpha(1);
			Animation translation = AnimationHelper.getInstance().getTranslationAnimation(200, 0, 500 + (i * 150), new OvershootInterpolator(), null);
			Animation fadeIn = AnimationHelper.getInstance().getFadeAnimation( 0, 1, 500 + (i * 150), null, null);

			AnimationSet animation = new AnimationSet(false);
			animation.addAnimation(fadeIn);
			animation.addAnimation(translation);
			edgeViewHolder.rltContacts.get(i).startAnimation(animation);
			((ContactProperties)edgeViewHolder.rltContacts.get(i).getTag(R.string.contact_properties_key)).resetProperties();
		}
	}

	private void onContactClicked(int position) {

		if(selectedContact==position){
//			edgeViewHolder.rltContacts.get(selectedContact).setScaleX(1/1.5f);
//			edgeViewHolder.rltContacts.get(selectedContact).setScaleY(1/1.5f);
			return;
		}
		// if(i<position){
		// translate = AnimationHelper.getInstance().getTranslationAnimation(0,
		// ((position-i)*25)-35, 500, new DecelerateInterpolator(), null);
		// } else if(i>position){
		// translate = AnimationHelper.getInstance().getTranslationAnimation(0,
		// ((i-position)*25)-35, 500, new DecelerateInterpolator(), null);
		// }

		
		Animation scale = AnimationHelper.getInstance().getScaleAnimation(1, 1.5f, 500, new OvershootInterpolator(),
				new AnimationListener() {
					@Override
					public void onAnimationStart(Animation animation) {
					}

					@Override
					public void onAnimationRepeat(Animation animation) {
					}

					@Override
					public void onAnimationEnd(Animation animation) {

					}
				});
		ContactProperties property = (ContactProperties)edgeViewHolder.rltContacts.get(position).getTag(R.string.contact_properties_key);
		Animation translation = AnimationHelper.getInstance().getTranslationAnimation(property.getCurrentDeltaPosX(), -35, 500, new DecelerateInterpolator(), null);
		AnimationSet animation = new AnimationSet(false);
		animation.addAnimation(scale);
		animation.addAnimation(translation);
		animation.setFillAfter(true);
		edgeViewHolder.rltContacts.get(position).startAnimation(animation);
		for (int i = 0; i < edgeViewHolder.rltContacts.size(); i++) {
			ContactProperties properties = (ContactProperties)edgeViewHolder.rltContacts.get(i).getTag(R.string.contact_properties_key);
			Animation translate = null;
			if (i < position) {
				float p = ((position - i) * 25) - 35;
				translate = AnimationHelper.getInstance().getTranslationAnimation(properties.getCurrentDeltaPosX(), p, 500, new DecelerateInterpolator(), null);
				properties.setCurrentDeltaPosX(p);
				edgeViewHolder.rltContacts.get(i).setTag(R.string.contact_properties_key,properties);
			} else if (i > position) {
				float p = ((i - position) * 25) - 35;
				translate = AnimationHelper.getInstance().getTranslationAnimation(properties.getCurrentDeltaPosX(), p, 500, new DecelerateInterpolator(), null);
				properties.setCurrentDeltaPosX(p);
				edgeViewHolder.rltContacts.get(i).setTag(R.string.contact_properties_key,properties);
			} else {
				properties.setCurrentDeltaPosX(-35);
				edgeViewHolder.rltContacts.get(i).setTag(R.string.contact_properties_key,properties);
			}
			if (i != position) {
				if(i==selectedContact){
					
					Animation scaleBack = AnimationHelper.getInstance().getScaleAnimation(1.5f, 1f, 500, new DecelerateInterpolator(), null);
					AnimationSet anim = new AnimationSet(false);
					anim.addAnimation(scaleBack);
					anim.addAnimation(translate);
					anim.setFillAfter(true);
					edgeViewHolder.rltContacts.get(i).startAnimation(anim);
				} else {
					
					edgeViewHolder.rltContacts.get(i).startAnimation(translate);
				}
			}
		}
		selectedContact = position;
	}
	
	private void hideContacts(){
		Animation scale = AnimationHelper.getInstance().getScaleAnimation(1, 0,
				1000, new OvershootInterpolator(), new AnimationListener() {

					@Override
					public void onAnimationStart(Animation animation) {
					}

					@Override
					public void onAnimationRepeat(Animation animation) {
					}

					@Override
					public void onAnimationEnd(Animation animation) {
						edgeViewHolder.lnrContacts.setVisibility(View.GONE);
						showButtons();
					}
				});
		edgeViewHolder.imgCloseContacts.startAnimation(scale);
		for (int i = 0; i < edgeViewHolder.rltContacts.size(); i++) {
			Animation anim = AnimationHelper.getInstance().getScaleAnimation(1,
					0, 500 + (i * 100), new OvershootInterpolator(), null);
			edgeViewHolder.rltContacts.get(i).startAnimation(anim);
		}
	}

	private void startContactsListHideAnimation() {
		isContactsListOpen = false;
		if(selectedContact!=-1){
			AnimationListener listener = new AnimationListener() {
				
				@Override
				public void onAnimationStart(Animation animation) {
				}
				
				@Override
				public void onAnimationRepeat(Animation animation) {
				}
				
				@Override
				public void onAnimationEnd(Animation animation) {
					hideContacts();
				}
			};
			for (int i = 0; i < edgeViewHolder.rltContacts.size(); i++) {
				ContactProperties properties = (ContactProperties)edgeViewHolder.rltContacts.get(i).getTag(R.string.contact_properties_key);
				Animation translate = null;
				translate = AnimationHelper.getInstance().getTranslationAnimation(properties.getCurrentDeltaPosX(), 0, 200, new DecelerateInterpolator(), null);
				properties.setCurrentDeltaPosX(0);
				edgeViewHolder.rltContacts.get(i).setTag(R.string.contact_properties_key,properties);
			
				if(i==selectedContact){
					Animation scaleBack = AnimationHelper.getInstance().getScaleAnimation(1.5f, 1f, 200, new DecelerateInterpolator(), null);
					AnimationSet anim = new AnimationSet(false);
					anim.addAnimation(scaleBack);
					anim.addAnimation(translate);
					anim.setFillAfter(true);
					anim.setAnimationListener(listener);
					edgeViewHolder.rltContacts.get(i).startAnimation(anim);
				} else {
					edgeViewHolder.rltContacts.get(i).startAnimation(translate);
				}
			
			}
			
		} else {
			hideContacts();
		}
		

	}

	private void updateBubbleView() {
		int targetScrollX = MIN_MOVE_FROM_RIGHT_TO_LEFT;
		if (paramsBubble.x < ((MIN_MOVE_FROM_RIGHT_TO_LEFT - MAX_MOVE_FROM_RIGHT_TO_LEFT) / 2)) {
			targetScrollX = MAX_MOVE_FROM_RIGHT_TO_LEFT;
		}
		scrollTo(targetScrollX);
	}

	private void hideButtons() {
		paramsBubble.flags = LayoutParamFlags;
		windowManager.updateViewLayout(edgeViewHolder.rootView, paramsBubble);
		Animation fadeOut = AnimationHelper.getInstance().getFadeAnimation(1,
				0, 100, new DecelerateInterpolator(), new AnimationListener() {
					@Override
					public void onAnimationStart(Animation animation) {
					}

					@Override
					public void onAnimationRepeat(Animation animation) {
					}

					@Override
					public void onAnimationEnd(Animation animation) {
						edgeButtonsViewHolder.showContactDrag.setAlpha(0);
						edgeButtonsViewHolder.showContactsList.setAlpha(0);
					}
				});
		edgeButtonsViewHolder.showContactDrag.startAnimation(fadeOut);
		edgeButtonsViewHolder.showContactsList.startAnimation(fadeOut);
	}

	void showButtons() {
		paramsBubble.flags = LayoutParamFlags
				| WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE;
		windowManager.updateViewLayout(edgeViewHolder.rootView, paramsBubble);
		Animation fadeIn = AnimationHelper.getInstance().getFadeAnimation(0, 1,
				500, new DecelerateInterpolator(), null);
		edgeButtonsViewHolder.showContactDrag.setAlpha(1);
		edgeButtonsViewHolder.showContactsList.setAlpha(1);
		edgeButtonsViewHolder.showContactDrag.startAnimation(fadeIn);
		edgeButtonsViewHolder.showContactsList.startAnimation(fadeIn);
	}

	void onEdgeBubbleOpenStarted() {
		if (isBubbleOpen) {
			return;
		}

		edgeViewHolder.rltContactImg.setVisibility(View.VISIBLE);
		Animation scale1 = AnimationHelper.getInstance().getScaleAnimation(0,
				1, 500, new OvershootInterpolator(), null);
		Animation scale2 = AnimationHelper.getInstance().getScaleAnimation(0,
				1, 800, new OvershootInterpolator(), null);
		edgeViewHolder.imgContactBg.startAnimation(scale1);
		edgeViewHolder.imgContact.startAnimation(scale2);
	}

	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		stopSelf();
	}

	void onEdgeBubbleOpen() {
		if (isBubbleOpen) {
			return;
		}
		isBubbleOpen = true;
		edgeViewHolder.rltEdge.setOnTouchListener(touchListener);
		Log.e(TAG, "onEdgeBubbleOpen");

		Log.e(TAG, "onEdgeBubbleOpenStarted");
		Animation translation = AnimationHelper.getInstance()
				.getTranslationAnimation(90, 0, 500,
						new DecelerateInterpolator(), null);
		Animation fadeIn = AnimationHelper.getInstance().getFadeAnimation(0, 1,
				1000, new DecelerateInterpolator(), null);

		AnimationSet animation = new AnimationSet(false); // change to false
		animation.addAnimation(fadeIn);
		animation.addAnimation(translation);

		edgeViewHolder.txtTitle.setAlpha(1);
		edgeViewHolder.txtTitle.startAnimation(animation);

		edgeViewHolder.txtEvent.setAlpha(1);
		edgeViewHolder.txtEvent.startAnimation(animation);

		edgeViewHolder.imgMsg.setAlpha(1f);
		edgeViewHolder.imgMsg.startAnimation(fadeIn);

		edgeViewHolder.imgCall.setAlpha(1f);
		edgeViewHolder.imgCall.startAnimation(fadeIn);

		edgeViewHolder.imgClose.setAlpha(1f);
		edgeViewHolder.imgClose.startAnimation(fadeIn);

		Animation fadeIn1 = AnimationHelper.getInstance().getFadeAnimation(0,
				1, 2000, new DecelerateInterpolator(), null);

		edgeViewHolder.txtTime.setAlpha(1);
		edgeViewHolder.txtTime.startAnimation(fadeIn1);
	}

	void onEdgeBubbleClose() {
		Log.e(TAG, "onEdgeBubbleClose");
		isBubbleOpen = false;
		showButtons();
		edgeViewHolder.txtTitle.setAlpha(0);
		edgeViewHolder.txtEvent.setAlpha(0);
		edgeViewHolder.txtTime.setAlpha(0);
		edgeViewHolder.imgMsg.setAlpha(0f);
		edgeViewHolder.imgCall.setAlpha(0f);
		edgeViewHolder.imgClose.setAlpha(0f);
		edgeViewHolder.imgContact.setScaleX(1);
		edgeViewHolder.imgContact.setScaleY(1);
		edgeViewHolder.imgContactBg.setScaleX(1);
		edgeViewHolder.imgContactBg.setScaleY(1);
		edgeViewHolder.rltContactImg.setVisibility(View.GONE);
		edgeViewHolder.rltEdge.setOnTouchListener(null);
		paramsBubble.y = (int) (50 * ViewFactory.getInstance().getHeightRatio());
		windowManager.updateViewLayout(edgeViewHolder.rootView, paramsBubble);
	}

	void startPopupCloseAnimation() {
		Animation scale1 = AnimationHelper.getInstance().getScaleAnimation(1,
				0, 50, new OvershootInterpolator(), null);
		edgeViewHolder.imgContact.startAnimation(scale1);

		Animation scale2 = AnimationHelper.getInstance().getScaleAnimation(1,
				0, 500, new OvershootInterpolator(), new AnimationListener() {

					@Override
					public void onAnimationStart(Animation animation) {
					}

					@Override
					public void onAnimationRepeat(Animation animation) {
					}

					@Override
					public void onAnimationEnd(Animation animation) {
						scrollTo(MIN_MOVE_FROM_RIGHT_TO_LEFT);
					}
				});
		edgeViewHolder.imgContactBg.startAnimation(scale2);
	}

	// private void scaleView(View v, float startScale, float endScale, int
	// duration, AnimationListener listener) {
	// Animation anim = new ScaleAnimation(
	// startScale, endScale,
	// startScale, endScale,
	// Animation.RELATIVE_TO_SELF, 0.5f,
	// Animation.RELATIVE_TO_SELF, 0.5f);
	// anim.setFillAfter(true);
	// anim.setDuration(duration);
	// anim.setInterpolator(new OvershootInterpolator());
	// anim.setAnimationListener(listener);
	// v.startAnimation(anim);
	// }
	//

	void scrollTo(final int targetScrollX) {
		ValueAnimator scrollAnimation = ValueAnimator.ofInt(paramsBubble.x,
				targetScrollX);
		scrollAnimation.setDuration(500);
		scrollAnimation.setInterpolator(new DecelerateInterpolator(5));
		scrollAnimation.addUpdateListener(new AnimatorUpdateListener() {
			@Override
			public void onAnimationUpdate(ValueAnimator animation) {
				paramsBubble.x = (Integer) animation.getAnimatedValue();
				windowManager.updateViewLayout(edgeViewHolder.rootView,
						paramsBubble);
			}
		});

		scrollAnimation.addListener(new AnimatorListener() {

			@Override
			public void onAnimationStart(Animator animation) {
				if (targetScrollX == MAX_MOVE_FROM_RIGHT_TO_LEFT) {
					onEdgeBubbleOpenStarted();
				}
			}

			@Override
			public void onAnimationRepeat(Animator animation) {
			}

			@Override
			public void onAnimationEnd(Animator animation) {
				if (targetScrollX == MAX_MOVE_FROM_RIGHT_TO_LEFT) {
					onEdgeBubbleOpen();
				} else {
					onEdgeBubbleClose();
				}
			}

			@Override
			public void onAnimationCancel(Animator animation) {

			}
		});

		scrollAnimation.start();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.imgClose:
			startPopupCloseAnimation();
			break;
		case R.id.imgCloseContacts:
			startContactsListHideAnimation();
			break;
		case R.id.imgMsg:
			Toast.makeText(this, "Message Pressed", Toast.LENGTH_SHORT).show();
			startPopupCloseAnimation();
			break;
		case R.id.imgCall:
			Toast.makeText(this, "Call Pressed", Toast.LENGTH_SHORT).show();
			startPopupCloseAnimation();
			break;
		default:
			break;
		}

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		windowManager.removeView(edgeViewHolder.rootView);
	}

}
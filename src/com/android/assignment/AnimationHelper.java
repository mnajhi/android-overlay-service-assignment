package com.android.assignment;


import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.Interpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;

public class AnimationHelper {
	private static AnimationHelper instance_ = null;
	public static AnimationHelper getInstance(){
		if(instance_==null){
			instance_ = new AnimationHelper();
		}
		return instance_;
	}
	
	public Animation getScaleAnimation(float startScale, float endScale, int duration, Interpolator i, AnimationListener listener) {
	    Animation anim = new ScaleAnimation(
	    		startScale, endScale,
	            startScale, endScale,
	            Animation.RELATIVE_TO_SELF, 0.5f,
	            Animation.RELATIVE_TO_SELF, 0.5f);
	    anim.setFillAfter(true);
	    anim.setDuration(duration);
	    if(i!=null){
	    	anim.setInterpolator(i);
	    }
	    anim.setAnimationListener(listener);
	    return anim;
	}
	
	public Animation getFadeAnimation(float start, float end, int duration, Interpolator i, AnimationListener listener){
		Animation anim = new AlphaAnimation(start, end);
		anim.setDuration(duration);
		anim.setFillAfter(true);
	    anim.setDuration(duration);
	    if(i!=null){
	    	anim.setInterpolator(i);
	    }
	    anim.setAnimationListener(listener);
	    return anim;
	}
	
	public Animation getRotationAnimation(float start, float end, int duration, Interpolator i, AnimationListener listener){
		Animation anim =new RotateAnimation(start, end, 
				Animation.RELATIVE_TO_SELF, 0.5f,
	            Animation.RELATIVE_TO_SELF, 0.5f);
		anim.setDuration(duration);
		anim.setFillAfter(true);
	    anim.setDuration(duration);
	    if(i!=null){
	    	anim.setInterpolator(i);
	    }
	    anim.setAnimationListener(listener);
	    return anim;
	}
	
	public Animation getTranslationAnimation(float start, float end, int duration, Interpolator i, AnimationListener listener){
		Animation anim = new TranslateAnimation(start*ViewFactory.getInstance().getWidthRatio(), end*ViewFactory.getInstance().getWidthRatio(), 0, 0);
		anim.setDuration(duration);
		anim.setFillAfter(true);
	    anim.setDuration(duration);
	    if(i!=null){
	    	anim.setInterpolator(i);
	    }
	    anim.setAnimationListener(listener);
	    return anim;
	}
}

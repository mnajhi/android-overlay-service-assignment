package com.android.assignment;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.TableRow;
import android.widget.TextView;

public class ViewFactory {
	private double standardWidth = 480;
	private double standardHeight = 800;
	private double standardDensity = 1.5f;
	private double widthRatio = 0;
	private double heightRatio = 0;
	private double minScalingFactor = 0;
	private static ViewFactory factory;

	private ViewFactory() {}

	public static ViewFactory getInstance() {
		if (factory == null) {
			factory = new ViewFactory();
		}
		return factory;
	}

	public void init(Activity activity) {
		DisplayMetrics displaymetrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		int height = displaymetrics.heightPixels;
		int width = displaymetrics.widthPixels;
		
		widthRatio = (width / standardWidth);
		heightRatio = (height / standardHeight);
		minScalingFactor = Math.min(widthRatio, heightRatio) * (standardDensity / activity.getResources().getDisplayMetrics().density);
	}

	public void scaleView(View view) {
		recursiveScaleView(view);
	}

	private void recursiveScaleView(View childView) {
		try{
			MarginLayoutParams linearParams = (MarginLayoutParams) childView
					.getLayoutParams();
	
			if (linearParams != null) {
				if (linearParams.width != MarginLayoutParams.WRAP_CONTENT) {
					linearParams.width *= widthRatio;
				}
				if (linearParams.width == 0 && childView instanceof TableRow) {
					linearParams.width = (int) (800 * widthRatio);
				}
				if (linearParams.height != MarginLayoutParams.WRAP_CONTENT) {
					linearParams.height *= heightRatio;
				}
	
				linearParams.leftMargin *= widthRatio;
				linearParams.topMargin *= heightRatio;
				linearParams.rightMargin *= widthRatio;
				linearParams.bottomMargin *= heightRatio;
				
	
				childView.setLayoutParams(linearParams);
			}
	
			if (childView instanceof ViewGroup) {
				int childCount = ((ViewGroup) childView).getChildCount();
				for (int i = 0; i < childCount; i++) {
					recursiveScaleView(((ViewGroup) childView).getChildAt(i));
				}
				return;
			} else if (childView instanceof TextView) {
				float textSize = ((TextView) childView).getTextSize();
				textSize *= minScalingFactor;
	
				((TextView) childView).setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
			}
		}catch(Exception e){}
	}

	public double getTextScaleFactor() {
		return minScalingFactor;
	}

	public Bitmap scaleBitmap(Bitmap bitmap, int scaleToWidth, int scaleToHeight) {
		Bitmap scaledBitmap = null;

		int width = bitmap.getWidth();
		int height = bitmap.getHeight();

		float widthRatio = (float) scaleToWidth / (float) width;
		float heightRatio = (float) scaleToHeight / (float) height;

		Matrix matrix = new Matrix();
		matrix.setScale(widthRatio, heightRatio);
		scaledBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix,
				false);

		return scaledBitmap;
	}

	public float getWidthRatio() {
		return (float) widthRatio;
	}

	public float getHeightRatio() {
		return (float) heightRatio;
	}

}
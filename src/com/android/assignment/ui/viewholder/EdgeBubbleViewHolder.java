package com.android.assignment.ui.viewholder;

import java.util.ArrayList;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.assignment.ContactProperties;
import com.android.assignment.R;

public class EdgeBubbleViewHolder extends BaseViewHolder {
	public View rootView;
//	public View showContactDrag;
//	public View showContactsList;
	public RelativeLayout rltEdge;
	public RelativeLayout rltContactImg;
	public ImageView imgContactBg;
	public ImageView imgContact;
	public ImageView imgClose;
	public TextView txtTitle;
	public TextView txtEvent;
	public TextView txtTime;
	public ImageView imgCall;
	public ImageView imgMsg;
	
	public LinearLayout lnrContacts;
	public ImageView imgCloseContacts;
	public ArrayList<RelativeLayout> rltContacts = new ArrayList<RelativeLayout>();
	
	private int rltContactsArray[]= {
			R.id.rltContact1,
			R.id.rltContact2,
			R.id.rltContact3,
			R.id.rltContact4,
			R.id.rltContact5,
	}; 

	public EdgeBubbleViewHolder(View rootView) {
		this.rootView = rootView;
//		showContactDrag = (View) rootView.findViewById(R.id.showContactDrag);
//		showContactsList = (View) rootView.findViewById(R.id.showContactsList);
		rltEdge = (RelativeLayout) rootView.findViewById(R.id.rltEdge);
		rltContactImg = (RelativeLayout) rootView.findViewById(R.id.rltContactImg);
		imgContactBg = (ImageView) rootView.findViewById(R.id.imgContactBg);
		imgContact = (ImageView) rootView.findViewById(R.id.imgContact);
		imgClose = (ImageView) rootView.findViewById(R.id.imgClose);
		txtTitle = (TextView) rootView.findViewById(R.id.txtTitle);
		txtEvent = (TextView) rootView.findViewById(R.id.txtEvent);
		txtTime = (TextView) rootView.findViewById(R.id.txtTime);
		imgCall = (ImageView) rootView.findViewById(R.id.imgCall);
		imgMsg = (ImageView) rootView.findViewById(R.id.imgMsg);
		lnrContacts = (LinearLayout) rootView.findViewById(R.id.lnrContacts);
		imgCloseContacts = (ImageView) rootView.findViewById(R.id.imgCloseContacts);
		
		for(int i=0 ; i <rltContactsArray.length ; i++){
			RelativeLayout rltContact = (RelativeLayout) rootView.findViewById(rltContactsArray[i]);
			ContactProperties properties = new ContactProperties();
			properties.setPosition(i);
			rltContact.setTag(R.string.contact_properties_key,properties);
			rltContacts.add(rltContact);
		}
	}
}

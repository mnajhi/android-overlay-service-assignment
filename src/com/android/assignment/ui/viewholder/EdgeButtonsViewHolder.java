package com.android.assignment.ui.viewholder;

import android.view.View;

import com.android.assignment.R;

public class EdgeButtonsViewHolder extends BaseViewHolder {
	public View rootView;
	public View showContactDrag;
	public View showContactsList;

	public EdgeButtonsViewHolder(View rootView) {
		this.rootView = rootView;
		showContactDrag = (View) rootView.findViewById(R.id.showContactDrag);
		showContactsList = (View) rootView.findViewById(R.id.showContactsList);
	}
}

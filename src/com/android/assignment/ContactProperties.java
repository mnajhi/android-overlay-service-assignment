package com.android.assignment;

public class ContactProperties {
	private float currentDeltaPosX = 0;
	private float currentDeltaPosY = 0;
	private int position = 0;
	private boolean isSelected = false;
	public float getCurrentDeltaPosX() {
		return currentDeltaPosX;
	}
	public void setCurrentDeltaPosX(float currentDeltaPosX) {
		this.currentDeltaPosX = currentDeltaPosX;
	}
	public float getCurrentDeltaPosY() {
		return currentDeltaPosY;
	}
	public void setCurrentDeltaPosY(float currentDeltaPosY) {
		this.currentDeltaPosY = currentDeltaPosY;
	}
	public boolean isSelected() {
		return isSelected;
	}
	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	public void resetProperties(){
		currentDeltaPosX = 0;
		currentDeltaPosY = 0;
		isSelected = false;
	}
}
